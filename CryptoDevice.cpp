#include "CryptoDevice.h"
#include <iostream>
#include <iomanip>
#include <bitset>

#include "modes.h"
#include "aes.h"
#include "filters.h"
#include "md5.h"
#include "hex.h"


typedef std::bitset<8> BYTE;
//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.

std::string CryptoDevice::encryptAES(std::string plainText)
{

	std::string cipherText;
	//
	// Create Cipher Text
	//
	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

std::string CryptoDevice::decryptAES(std::string cipherText)
{

	std::string decryptedText;
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}

bool CryptoDevice::authenticate(std::string username, std::string password)
{
	CryptoPP::MD5 hash;
	CryptoPP::byte digest[CryptoPP::MD5::DIGESTSIZE];
	std::string usernameTwo = "007";
	std::string passCompare = "anonymous123";
	hash.CalculateDigest(digest, (CryptoPP::byte*)passCompare.c_str(), passCompare.length());

	CryptoPP::HexEncoder encoder;
	std::string output;
	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();

	hash.CalculateDigest(digest, (CryptoPP::byte*)password.c_str(), password.length());

	std::string outputTwo;
	encoder.Attach(new CryptoPP::StringSink(outputTwo));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();
	if (output == outputTwo && username == usernameTwo)
		return true;

	return false;
}